

// Header Files
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "nvm_lpm.h"
#include "crc.h"  // CRC32




//-----------------------------------------------------------------------------
SPage_t data[ 256 ];
static SCache_t pageCache;


//-----------------------------------------------------------------------------
uint32_t pageCRC32( SPage_t *page ) {

  size_t length = sizeof( pageCache.page.rx );
  return CRC32( &pageCache.page.rx, length );

};  // end pageCRC32


//-----------------------------------------------------------------------------
void dump_page( SPage_t *page ) {

  size_t offset = 0;
  uint64_t longId = 0;

  for ( ; offset < RX_PER_PAGE; ++offset ) {
    longId = page->rx[ offset ].uint64;
    printf( "%d %u\n", offset, longId );
  };  // end for loop

  printf( "CRC: %u\n\n", page->crc32 );

};  // end dump_page


//-----------------------------------------------------------------------------
void clearPageCache( ) {

  size_t length = sizeof( pageCache.page );
  memset( &pageCache.page, 0U, length );
  pageCache.page.crc32 = pageCRC32( &pageCache.page );

};  // end clearPageCache


//-----------------------------------------------------------------------------
bool checkCRC32( ) {

  uint32_t crc32 = pageCRC32( &pageCache.page );

  printf( "checkCRC32: crc32 = %u | pageCache.page.crc32 = %u\n\n", crc32, pageCache.page.crc32 );

  return ( crc32 == pageCache.page.crc32 );

};  // end checkCRC32


//------------------------------------------------------------------------------
void loadPageToCache( uint16_t index ) {

  void *dest = &pageCache.page.rx;
  void *src = &data[ index ].rx;
  size_t length = sizeof( pageCache.page.rx );

  printf( "loading page at index = %d\n\n", index );

  memcpy( dest, src, length );
  pageCache.index = index;

  if ( !checkCRC32( ) ) clearPageCache( );

};  // end loadPageToCache


//-----------------------------------------------------------------------------
void writePageCache( ) {

  void *dest = &data[ pageCache.index ];
  void *src = &pageCache.page;
  size_t length = sizeof( pageCache.page );

  pageCache.page.padding = 0U;  // clear padding value
  pageCache.page.crc32 = pageCRC32( &pageCache.page );
  memcpy( dest, src, length );

};  // end writePageCache


//-----------------------------------------------------------------------------
Place_t calculatePageAndOffset( uint16_t sTableIndex ) {

  Place_t place = {
    .index = sTableIndex / RX_PER_PAGE,
    .offset = sTableIndex % RX_PER_PAGE
  };  // end return

  place.offset = sTableIndex - ( place.index * RX_PER_PAGE );
  return place;

};  // end calculatePage


//-----------------------------------------------------------------------------
uint8_t offsetInCache( uint16_t sTableIndex ) {

  Place_t place = calculatePageAndOffset( sTableIndex );
  if ( pageCache.index != place.index ) loadPageToCache( place.index );
  return place.offset;

};  // end offsetInCache


//-----------------------------------------------------------------------------
ExtdAddr_t rxFromNvm( uint16_t sTableIndex ) {

  uint8_t offset = offsetInCache( sTableIndex );
  ExtdAddr_t addr = { .uint64 = pageCache.page.rx[ offset ].uint64 };
  return addr;

};  // end rxFromNvm


//-----------------------------------------------------------------------------
void rxToNvm( uint16_t sTableIndex, ExtdAddr_t longId ) {

  uint8_t offset = offsetInCache( sTableIndex );
  pageCache.page.rx[ offset ].uint64 = longId.uint64;
  writePageCache( );

};  // end rxToNvm


//-----------------------------------------------------------------------------
void fill_page( SPage_t *page ) {

  size_t iter = 0;
  for ( ; iter < RX_PER_PAGE; ++iter )
    page->rx[ iter ].uint64 = iter + 8;
  page->crc32 = CRC32( page->rx, sizeof( page->rx ) );
  page->padding = 0;

  dump_page( page );

};  // end fill_page


//-----------------------------------------------------------------------------
ExtdAddr_t createExtdAddr( uint64_t id ) {

  ExtdAddr_t addr;
  addr.uint64 = id;
  return addr;

};  // end createExtdAddr


//-----------------------------------------------------------------------------
int main( void ) {

  ExtdAddr_t one = rxFromNvm( 0 );  // read first value in slot table

  fill_page( &data[ 0 ] );

  one.uint64 = 123123123;
  rxToNvm( 1, one );

  dump_page( &data[ 0 ] );
  one = rxFromNvm( 1 );

  dump_page( &data[ 0 ] );

  one = rxFromNvm( 32 );

  dump_page( &data[ 0 ] );
  one = rxFromNvm( 1 );
  dump_page( &pageCache.page );

  return 0;

};  // end main
