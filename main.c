

//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include "table.h"
#include "zap_page_cache.h"
#include "as_hex.h"

//-----------------------------------------------------------------------------
void create_table( ) {

  EepromCtor();  // init NVM
  ExtdAddr_t longId = { .uint64 = 256 };
  for ( size_t index = 0; index < LIST_SIZE; ++index ) {
    SManagerAssign( longId, index );
    longId.uint64 += 256;
  };  // end for loop
  dump_list();  // debug output

};  // end create_table


//-----------------------------------------------------------------------------
void delete_table( ) {

  list_del_all();

};  // end delete_table


//-----------------------------------------------------------------------------
void assign( uint64_t uint64, zTableIndex_t zapIndex ) {

  ExtdAddr_t longId = { .uint64 = uint64 };
  dump_cache();
  SManagerAssign( longId, zapIndex );
  dump_cache();

};  // end assign


void dump() {

  for ( zTableIndex_t zapIndex = 0; zapIndex < LIST_SIZE; ++zapIndex )
    printf( "%02d: %s\n", zapIndex, as_hex( ReadZapFromNvm( zapIndex ) ) );

};  // end dump


//-----------------------------------------------------------------------------
int main( void ) {

  char *buffer = "0123456789ABCDEF0123456789ABCDEF";
  ExtdAddr_t longId = { .uint64 = 0x88 };

  create_table();

  for ( size_t nvmIndex = 0; nvmIndex < 7; ++nvmIndex )
    dump_page( nvmIndex );


  assign( 0x123, 4 );
  assign( 0x321, 8 );

  dump();

  delete_table();

  return 0;

};  // end main
