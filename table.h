#ifndef __TABLE_H__
#define __TABLE_H__


//-----------------------------------------------------------------------------
#include <stdint.h>
#include "zap_table.h"  // zTableIndex_t
#include "rx.h"


//-----------------------------------------------------------------------------
#define LIST_SIZE 200

//-----------------------------------------------------------------------------
void SManagerAssign( ExtdAddr_t, zTableIndex_t );
void SManagerRemove( zTableIndex_t );

void list_add( uint64_t, uint16_t );
void list_del( uint16_t );
void list_del_all();
void dump_list();

#endif  // __TABLE_H__
