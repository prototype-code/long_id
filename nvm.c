

// Header Files
//-----------------------------------------------------------------------------
#include <stdio.h>  // printf
#include <string.h>  // memcpy
#include "nvm.h"


static NvmPage_t NVM[ NVM_PAGE_COUNT ];
//static uint8_t NVM[ NVM_PAGE_COUNT ][ NVM_PAGE_SIZE ];




//-----------------------------------------------------------------------------
void NvmCtor( ) {

  for ( size_t index = 0; index < NVM_PAGE_COUNT; ++index )
    memset( NVM[ index ], 0U, NVM_PAGE_SIZE );

};  // end NvmCtor


//-----------------------------------------------------------------------------
CotaError_t WriteNvmPage( NvmPage_t const *page, uint16_t index ) {

  if ( page == NULL ) return COTA_ERROR_NONE;
  if ( index >= NVM_PAGE_COUNT ) return COTA_ERROR_NVM_WRITE;

  memcpy( NVM[ index ], page, sizeof( NvmPage_t ) );

  return COTA_ERROR_NONE;

};  // end WriteNvmPage


//-----------------------------------------------------------------------------
CotaError_t ReadNvmPage( NvmPage_t *page, uint16_t index ) {

  if ( index >= NVM_PAGE_COUNT ) return COTA_ERROR_NVM_READ;
  memcpy( page, NVM[ index ], sizeof( NvmPage_t ) );

  return COTA_ERROR_NONE;

};  // end ReadNvmPage
