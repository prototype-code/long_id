/**
 * @file       crc.h
 *
 * @brief      This file contains CRC-32 module header
 *
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef CRC_H
#define CRC_H

/* Includes -----------------------------------------------------------------*/
#include <stdint.h>


// Macro short cut that casts data as a pointer to bytes
#define CRC32( data, size ) Crc32( ( uint8_t* )( data ), size )


/* Public function prototypes -----------------------------------------------*/
uint32_t Crc32(uint8_t *data, uint32_t size);

#endif  // #ifndef CRC_H
