#ifndef __ZAP_NVM_H__
#define __ZAP_NVM_H__


// Header Files
//-----------------------------------------------------------------------------
#include "error.h"  // CotaError_t
#include "zap_table.h"  // zTableIndex_t
#include "rx.h"  // rx_t
#include "nvm.h"  // NVM_PAGE_SIZE
#include "eeprom_driver.h"


//-----------------------------------------------------------------------------
#define ZAPS_PER_PAGE 4
#define ZAP_PAGE_PADDING 4


// Prototype Functions
//-----------------------------------------------------------------------------
CotaError_t WriteZapToNvm( uint64_t uint64, zTableIndex_t );
uint64_t ReadZapFromNvm( zTableIndex_t );
void dump_page( uint16_t );

//-----------------------------------------------------------------------------
typedef struct _NvmZapPage_t {
  ExtdAddr_t zap[ ZAPS_PER_PAGE ];
  uint32_t crc32;  // 4 byte CRC encoding
  uint8_t padding[ ZAP_PAGE_PADDING ];
} NvmZapPage_t;


//-----------------------------------------------------------------------------
typedef struct _NvmPageCache_t {
  union {
    NvmZapPage_t page;
    NvmPage_t nvmPage;
  };  // end union
  uint16_t index;
} NvmPageCache_t;


//-----------------------------------------------------------------------------
typedef struct _Place_t {
  uint16_t index;
  uint8_t offset;
} Place_t;


#endif  // __ZAP_NVM_H__
