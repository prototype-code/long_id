/**
 * @file       crc.c
 *
 * @brief      This file contains CRC-32 algorithm implementation
 *
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


/* Includes -----------------------------------------------------------------*/
#include "crc.h"


/* Private define -----------------------------------------------------------*/

/**
 *  CRC-32 polynomial used by ISO 3309 (HDLC), ANSI X3.66 (ADCCP), FED-STD-1003,
 *  ITU-T V.42, ISO/IEC/IEEE 802-3 (Ethernet), SATA, MPEG-2, PKZIP, Gzip,
 *  POSIX cksum, PNG, ZMODEM
 */
#define CRC32_POLY    0xEDB88320



/** Functions ---------------------------------------------------------------*/

/**
 * @brief  Function that calculates CRC-32 checksum for a buffer
 *
 * @param  data  Buffer with bytes to compute the checksum for
 * @param  size  The number of bytes in the buffer
 *
 * This function uses the same logic as ISO 3309 (HDLC), ANSI X3.66 (ADCCP), FED-STD-1003,
 * ITU-T V.42, ISO/IEC/IEEE 802-3 (Ethernet), SATA, MPEG-2, PKZIP, Gzip, POSIX cksum, PNG, ZMODEM
 *
 * @note    The ST MCU contains a hardware CRC calculation module that may be able to replace
 *          this function in the future, but, for now, since this function is not going to be
 *          used very often, the use of the hardware module seems unnecessary.
 *
 * @return  CRC-32 checksum of the bytes in the buffer
 */
uint32_t Crc32(uint8_t *data, uint32_t size)
{
    uint32_t r = ~0;
    uint8_t *end = data + size;
    uint32_t t;

    while (data < end)
    {
        r ^= *data++;

        for (uint32_t i = 0; i < 8; i++)
        {
            t = ~((r & 1) - 1);
            r = (r >> 1) ^ (CRC32_POLY & t);
        }
    }

    return ~r;
}
