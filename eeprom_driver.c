

// Header Files
//-----------------------------------------------------------------------------
#include <string.h>
#include "eeprom_driver.h"

#define ZAP_DATA_START &eeprom[ 0 ]

static uint8_t eeprom[ EEPROM_PAGE_SIZE * 32 ];



//-----------------------------------------------------------------------------
void EepromCtor( ) {

  uint8_t value = 65;

  for ( size_t index = 0; index < EEPROM_PAGE_COUNT; ++index )
    memset( &eeprom[ index * EEPROM_PAGE_SIZE ], value++, EEPROM_PAGE_SIZE );

};  // end EepromCtor


//-----------------------------------------------------------------------------
CotaError_t EepromWrite( uint64_t address, uint8_t* data, uint32_t size ) {

  memcpy( &eeprom[ address ], data, (size_t)( size ) );
  return COTA_ERROR_NONE;

};  // end EepromWrite



//-----------------------------------------------------------------------------
CotaError_t EepromRead( uint64_t address, uint8_t* data, uint32_t size ) {

  memcpy( data, &eeprom[ address ], (size_t)( size ) );
  return COTA_ERROR_NONE;

};  // end EepromWrite
