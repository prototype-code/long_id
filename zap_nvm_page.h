#ifndef __ZAP_NVM_PAGE_H__
#define __ZAP_NVM_PAGE_H__

//
// @file       zap_nvm_page.h
//
// @brief      Defines the macros, structures and exported functions that
//             are used with reading and writing Zap data to the NMV.
//
// @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
//             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
//             STRICTLY PROHIBITED.  COPYRIGHT 2021 OSSIA INC. (SUBJECT TO LIMITED
//             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
//


// Header Files
#include "packed.h"  // packed_struct
#include "rx.h"
#include "eeprom_driver.h"  // Eeprom functions


// Macros
#define ZAP_NVM_ADDRESS EEPROM_PAGE_SIZE * 2
#define ZAP_PAGE_PADDING 4
#define ZAPS_PER_PAGE 31  // leave room for CRC and padding
#define ZAP_PAGE_COUNT 7
#define ZAP_COUNT ZAP_PAGE_COUNT * ZAPS_PER_PAGE


/// @brief Defines how Zap data is stored in a page of of the NVM.
typedef packed_struct _NvmZapPage_t {
  ExtdAddr_t zap[ ZAPS_PER_PAGE ];
  uint32_t crc32;  // the CRC32 is 4 bytes long
  uint8_t padding[ ZAP_PAGE_PADDING ];
} NvmZapPage_t;


// Function Prototypes
CotaError_t WriteZapPageToNvm( NvmZapPage_t * const, uint16_t );
CotaError_t ReadZapPageFromNvm( NvmZapPage_t *, uint16_t );


#endif  // __ZAP_NVM_PAGE_H__
