#ifndef __NVM_H__
#define __NVM_H__

// Header Files
//-----------------------------------------------------------------------------
#include <stdint.h>  // uint8_t
#include "error.h"  // CotaError_t


// How large is a page
#define NVM_PAGE_SIZE 32
#define NVM_PAGE_COUNT 4

// Specific types for use with NVM
typedef uint8_t NvmPage_t[ NVM_PAGE_SIZE ];


// Prototype Functions
//-----------------------------------------------------------------------------
void NvmCtor( );
CotaError_t WriteNvmPage( NvmPage_t const *, uint16_t );
CotaError_t ReadNvmPage( NvmPage_t *, uint16_t );


#endif  // __NVM_H__
