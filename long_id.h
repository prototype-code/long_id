// long_id.h

#ifndef __LONG_ID_H__
#define __LONG_ID_H__


#include "packed.h"  // packed_struct

//-----------------------------------------------------------------------------
typedef packed_struct _ExtdAddr_t {
  union {
    uint8_t bytes[ 8 ];
    uint64_t id;
  };  // end union
} ExtdAddr_t;


#endif  // __LONG_ID_H__
