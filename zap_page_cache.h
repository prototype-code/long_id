#ifndef __ZAP_PAGE_CACHE_H__
#define __ZAP_PAGE_CACHE_H__

//
// @file       zap_page_cache.h
//
// @brief      Defines the structures and exported functions for reading and writing
//             Zap data to EEPROM.
//
// @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
//             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
//             STRICTLY PROHIBITED.  COPYRIGHT 2021 OSSIA INC. (SUBJECT TO LIMITED
//             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
//


// Header Files
#include "error.h"
#include "zap_nvm_page.h"
#include "table.h"  // zTableIndex_t


/// @brief Holds a single page of data read from the NVM plus the index of that page.
typedef struct _ZapPageCache_t {
  NvmZapPage_t page;  // A single page of data from the NVM
  uint16_t nvmIndex;  // index into the Zap data in NVM
} ZapPageCache_t;


/// @brief Holds the index of a page of memory in the NVM and the offset in
/// that page of an index in the Zap Table.
typedef struct _Place_t {
  uint16_t nvmIndex;  // of the page in the NVM
  uint16_t offset;  // of the specific Zap in the NVM Page
} Place_t;


// Function Prototypes
CotaError_t WriteZapToNvm( ExtdAddr_t, zTableIndex_t );
ExtdAddr_t ReadZapFromNvm( zTableIndex_t );

void dump_cache( );  // print contents of a page in eeprom
void dump_page( uint16_t );  // print contents of a page from eeprom

#endif  // __ZAP_PAGE_CACHE_HH__
