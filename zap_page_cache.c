
//
// @file       zap_page_cache.c
//
// @brief      Defines the high level functions used to read and write Zap Table data
//             in the EEPROM.  Uses a calculated CRC32 value to verify correctness.
//
// @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
//             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
//             STRICTLY PROHIBITED.  COPYRIGHT 2021 OSSIA INC. (SUBJECT TO LIMITED
//             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
//


// Header Files
#include <stdio.h>  // printf
#include <stdbool.h>  // bool
#include <string.h>
#include "zap_page_cache.h"
#include "as_hex.h"
#include "crc.h"


#define NVM_PAGE_SIZE EEPROM_PAGE_SIZE
#define UNUSED_NVM_PAGE 8


/// @brief A cache holding the last used NVM page with
/// data from the Zap Table.
static ZapPageCache_t PageCache;


static void dump_place( Place_t place ) {
  printf( "Place: nvmIndex = %02d, offset = %02d\n", place.nvmIndex, place.offset );
};  // end dump_place


/// @brief Use the CRC32 function to generate a value for all bytes in the page.
/// @param None
/// @return An unsigned 32 bit CRC value.
static uint32_t calculatePageCacheCRC( ) {

  static size_t length = sizeof( ExtdAddr_t ) * ZAPS_PER_PAGE;
  return CRC32( PageCache.page.zap, length );

};  // end calculatePageCacheCRC


/// @brief Compare the page CRC32 value with the page data.
/// @param None
/// @return True if the two values match, False if they don't.
static bool checkPageCacheCRC( ) {

  return ( calculatePageCacheCRC() == PageCache.page.crc32 );

};  // end checkPageCacheCRC


/// @brief Calculate the NVM page and offset that stores the RX long ID.
/// @param zapIndex An index in the Zap Table.
/// @return A struct holding both the index of the NVM page and the offset
/// in that page where the referenced zapIndex can be found.
static Place_t calculatePlaceInNvm( zTableIndex_t zapIndex ) {

  Place_t place = {
    .nvmIndex = zapIndex / ZAPS_PER_PAGE,
    .offset = 0U
  };  // end place

  place.offset = zapIndex - ( place.nvmIndex * ZAPS_PER_PAGE );
  return place;

};  // end calculatePlaceInNvm


/// @brief Clear all values from the PageCache.
/// @param None
/// @return None
static void clearPageCache( ) {

  memset( &PageCache.page, 0U, sizeof( PageCache ) );
  PageCache.page.crc32 = calculatePageCacheCRC();
  PageCache.nvmIndex = UNUSED_NVM_PAGE;

};  // end clearPageCache


/// @brief Read from NVM a page holding Zap Table data and store it in the cache.
/// @param nvmIndex The index in the NVM of the page.
/// @return None.
static void loadPageCache( uint16_t nvmIndex ) {

  if ( PageCache.nvmIndex == nvmIndex ) return;

  PageCache.nvmIndex = nvmIndex;
  CotaError_t result = ReadZapPageFromNvm( &PageCache.page, nvmIndex );
  if ( ( result != COTA_ERROR_NONE ) || ( !checkPageCacheCRC() ) )  clearPageCache();

};  // end loadPageCache


/// @brief Update one of the Zaps stored in the Page Cache then update the page CRC32.
/// @param longId The long ID of the RX.
/// @param offset The location in the page where the longId will be stored.
/// @return None
static void updatePageCache( ExtdAddr_t longId, uint16_t offset ) {

  PageCache.page.zap[ offset ] = longId;
  PageCache.page.crc32 = calculatePageCacheCRC();

};  // end updatePageCache


/// @brief
CotaError_t WriteZapToNvm( ExtdAddr_t longId, zTableIndex_t zapIndex ) {

  if ( zapIndex >= LIST_SIZE ) return COTA_ERROR_ZAP_MAXIMUM;
  printf( "WriteZapToNvm: %s at index %d\n", as_hex( longId ), zapIndex );
  Place_t place = calculatePlaceInNvm( zapIndex );

  loadPageCache( place.nvmIndex );
  updatePageCache( longId, place.offset );
  return WriteZapPageToNvm( &PageCache.page, place.nvmIndex );

};  // end WriteZapToNvm


/// @brief Read a specific RX long ID from the NVM by loading the NVM page
/// into the Page Cache if necessary.
/// @param zapIndex The index in the Zap Table of the RX.
/// @return An ExtdAddr_t object with the RX long ID.
ExtdAddr_t ReadZapFromNvm( zTableIndex_t zapIndex ) {

  ExtdAddr_t longId = { .uint64 = 0U };
  if ( zapIndex >= LIST_SIZE ) return longId;
  Place_t place = calculatePlaceInNvm( zapIndex );

  loadPageCache( place.nvmIndex );
  longId = PageCache.page.zap[ place.offset ];

  return longId;

};  // end ReadZapFromNvm




// DEBUGGING ONLY
static void dump( NvmZapPage_t* page ) {

  for ( size_t offset = 0; offset < ZAPS_PER_PAGE; ++offset )
    printf( "%02d: %s\n", offset, as_hex( page->zap[ offset ] ) );

  printf ( "CRC32 = %u\n\n", page->crc32 );

};  // end dump


// DEBUGGING ONLY
void dump_cache( ) {

  printf( "Page Cache : NVM Index %02d\n", PageCache.nvmIndex );
  dump( &PageCache.page );

};  // end dump_cache


// DEBUGGING ONLY
void dump_page( uint16_t nvmIndex ) {

  NvmZapPage_t page;
  ReadZapPageFromNvm( &page, nvmIndex );

  printf( "NVM page # %02d\n", nvmIndex );
  dump( &page );

};  // end dump_page
