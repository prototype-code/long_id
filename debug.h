#ifndef __DEBUG_HH__
#define __DEBUG_HH__

//-----------------------------------------------------------------------------
#include "zap_nvm.h"  // NvmZapPage_t


//-----------------------------------------------------------------------------
void dump_page( NvmZapPage_t * const );
void dump_nvm();


#endif  // __DEBUG_HH__
