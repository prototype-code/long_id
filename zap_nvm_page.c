

#include "zap_nvm_page.h"


/// @brief
CotaError_t WriteZapPageToNvm( NvmZapPage_t * const page, uint16_t nvmIndex ) {

  uint32_t address = ( EEPROM_PAGE_SIZE * nvmIndex ) + ZAP_NVM_ADDRESS;
  return EepromWrite( address, (uint8_t*)( page ), EEPROM_PAGE_SIZE );

};  // end WriteZapPageToNvm


/// @brief
CotaError_t ReadZapPageFromNvm( NvmZapPage_t *page, uint16_t nvmIndex ) {

  uint32_t address = ( EEPROM_PAGE_SIZE * nvmIndex ) + ZAP_NVM_ADDRESS;
  return EepromRead( address, (uint8_t*)( page ), EEPROM_PAGE_SIZE );

};  // end ReadZapPageFromNvm
