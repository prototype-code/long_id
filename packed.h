// packed.h

#ifndef __PACKED_H__
#define __PACKED_H__

#if defined( __GNUC__ )
  #define PACKED_STRUCT struct __attribute__(( __packed__ ))
#else  // ewarm
  #define PACKED_STRUCT __packed struct
#endif  // if gnuc or ewarm

// USE:
// typedef PACKED_STRUCT _Sample_T {
//   uint8_t one;
//   uint32_t two;
//   union {
//     bool test;
//     uint8_t value;
//   };  // union
// } Sample_t;
//


#endif  // __PACKED_H__
