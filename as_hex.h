#ifndef __AS_HEX_H__
#define __AS_HEX_H__

#include "rx.h"  // ExtdAddr_t

//-----------------------------------------------------------------------------
#define AS_HEX_LENGTH 64


//-----------------------------------------------------------------------------
char * const as_hex( ExtdAddr_t );


#endif  // __AS_HEX_H__
