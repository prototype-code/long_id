#ifndef __EEPROM_DRIVER_H__
#define __EEPROM_DRIVER_H__


#include <stdint.h>
#include "error.h"  // CotaError_t

#define EEPROM_PAGE_SIZE 256
#define EEPROM_PAGE_COUNT 8
#define EEPROM_SIZE EEPROM_PAGE_SIZE * EEPROM_PAGE_COUNT

void EepromCtor();
void dump_page( uint16_t );

CotaError_t EepromWrite( uint64_t, uint8_t*, uint32_t );
CotaError_t EepromRead( uint64_t, uint8_t*, uint32_t );


#endif  // __EEPROM_DRIVER_H__
