
cc := /usr/bin/gcc
flags := -g -Wfatal-errors -Werror


# Rules
%.d : %.c ; $(cc) -MM -MT $(@:.d=.o) $(flags) $< > $@
%.o : %.c ; $(cc) $(flags) -c -o $@ $<


target := test


src := \
  main.c \
  table.c \
  crc.c \
  as_hex.c \
  eeprom_driver.c \
  zap_page_cache.c \
  zap_nvm_page.c

objs := $(src:.c=.o)
deps := $(src:.c=.d)

all : $(target)

# Only include depenents if goal is not clean
ifneq ( $(MAKECMDGOALS), clean )
  include $(deps)  # add in dependents
endif

$(target) : $(objs) $(deps) ; $(cc) $(flags) -o $@ $(objs)

run : $(target) ; @./cache

.PHONY : clean
clean : ; rm --force *.o *.d $(target)
