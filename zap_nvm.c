
// Header Files
#include <stdio.h>
#include <stdbool.h>  // bool
#include <string.h>  // memset
#include "zap_nvm.h"
#include "crc.h"  // CRC32
#include "as_hex.h"


//-----------------------------------------------------------------------------
static NvmPageCache_t PageCache;



//-----------------------------------------------------------------------------
static uint32_t pageCRC32( ) {

  size_t length = sizeof( PageCache.page.zap );
  return CRC32( &PageCache.page.zap, length );

};  // end pageCRC32


//-----------------------------------------------------------------------------
bool checkCRC32( ) {

  uint32_t crc32 = pageCRC32( );
  return ( crc32 == PageCache.page.crc32 );

};  // end checkCRC32


//-----------------------------------------------------------------------------
static void clearPageCache( ) {

  size_t length = sizeof( PageCache.page );
  memset( &PageCache.page, 0U, length );
  PageCache.page.crc32 = pageCRC32( &PageCache.page );

};  // end clearPageCache


//-----------------------------------------------------------------------------
static void loadPageCache( uint16_t index ) {

  void *dest = &PageCache.page.zap;
  //  WriteNvmPage( &PageCache.nvmPage, index );
  ReadNvmPage( dest, index );

};  // end loadPageCache


//-----------------------------------------------------------------------------
Place_t calculatePageAndOffset( zTableIndex_t zapIndex ) {

  Place_t place = {
    .index = zapIndex / ZAPS_PER_PAGE,
    .offset = zapIndex % ZAPS_PER_PAGE
  };  // end place

  place.offset = zapIndex - ( place.index * ZAPS_PER_PAGE );
  return place;

};  // end calculatePageAndOffset


//-----------------------------------------------------------------------------
uint8_t offsetInCache( zTableIndex_t zapIndex ) {

  Place_t place = calculatePageAndOffset( zapIndex );
  if ( PageCache.index != place.index ) loadPageCache( place.index );
  return place.offset;

};  // end offsetInCache


//-----------------------------------------------------------------------------
CotaError_t WriteZapToNvm( uint64_t longId, zTableIndex_t zapIndex ) {

  CotaError_t result = COTA_ERROR_NONE;  // default

  uint8_t cacheOffset = offsetInCache( zapIndex );

  PageCache.page.zap[ cacheOffset ].uint64 = longId;
  WriteNvmPage( &PageCache.nvmPage, PageCache.index );

  // Calculate page in NVM for zap
  // Store zap in cache (loading new cache if necesarry)

  return result;

};  // end WriteZapToNvm


//-----------------------------------------------------------------------------
uint64_t ReadZapFromNvm( zTableIndex_t zapIndex ) {

  uint64_t longId = UNUSED_ZAP;

  // Calculate page in NVM where zap goes
  // Load page if necesarry
  // Read zap from cache

  return longId;

};  // end ReadZapFromNvm



//-----------------------------------------------------------------------------
void dump_page( uint16_t nvmIndex ) {

  if ( PageCache.index != nvmIndex ) loadPageCache( nvmIndex );
  printf( "NVM Page #%02d\n", nvmIndex );
  for( size_t zap = 0; zap < ZAPS_PER_PAGE; ++zap )
    printf( "%02d: %s\n", zap, as_hex( PageCache.page.zap[ zap ].uint64 ) );
  printf( "\n" );

  };  // end dump_page
