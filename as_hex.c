

//-----------------------------------------------------------------------------
#include <stdio.h>  // sprintf
#include <string.h>  // memset
#include <stdint.h>  // uint64_t
#include "as_hex.h"


//-----------------------------------------------------------------------------
static char buffer[ AS_HEX_LENGTH ];


//-----------------------------------------------------------------------------
char * const as_hex( ExtdAddr_t longId ) {

  memset( buffer, 0U, AS_HEX_LENGTH );
  snprintf( buffer, AS_HEX_LENGTH, "0x%08x", longId.uint64 );
  return buffer;

};  // end AsHex
