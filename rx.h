#ifndef __RX_H__
#define __RX_H__

// Header Files
//-----------------------------------------------------------------------------
#include <stdint.h>  // uint8_t and uint64_t

//-----------------------------------------------------------------------------
#define UNUSED_ZAP INT64_MAX



typedef struct _ExtdAddr_t {
  union {
    uint8_t bytes[ 8 ];
    uint64_t uint64;
  };  // end union
} ExtdAddr_t;


typedef struct _rx_t {
  ExtdAddr_t longId;
} rx_t;


#endif  // __RX_H__
