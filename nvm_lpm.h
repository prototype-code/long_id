

#ifndef __NVM_LPM_H__
#define __NVM_LPM_H__

// Header Files
//-----------------------------------------------------------------------------
#include <stdint.h>  // uint*_t
#include "rx.h"  // ExtdAddr_


#define NUM_OF_PAGES 256
#define RX_PER_PAGE 4


//-----------------------------------------------------------------------------
typedef packed_struct _SPage_t {
  ExtdAddr_t rx[ RX_PER_PAGE ];  // 31 * 8 bytes = 248 bytes
  uint32_t crc32;  // 4 bytes
  uint32_t padding;  // 4 bytes
} SPage_t;


//-----------------------------------------------------------------------------
typedef struct _SCache_t {
  SPage_t page;
  uint16_t index;
} SCache_t;


//-----------------------------------------------------------------------------
typedef struct __Place_t {
  uint16_t index;
  uint8_t offset;
} Place_t;


#endif  // __NVM_LPM_H__
