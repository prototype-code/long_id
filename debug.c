
// Header Files
//-----------------------------------------------------------------------------
#include <stdio.h>  // printf
#include "debug.h"


//-----------------------------------------------------------------------------
void dump_page( NvmZapPage_t* const page ) {

  for ( size_t index = 0; index < ZAPS_PER_PAGE; ++index )
    printf( "%d: %u\n", index, page->zap[ index ] );

};  // end dump_page
