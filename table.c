

//-----------------------------------------------------------------------------
#include <stddef.h>  // size_t
#include <stdlib.h>  // malloc free
#include <stdio.h>  // printf
#include "table.h"
#include "as_hex.h"
#include "rx.h"  // rx_t
#include "zap_page_cache.h"

// list of rx objects
//-----------------------------------------------------------------------------
static rx_t *the_list[ LIST_SIZE ];


//-----------------------------------------------------------------------------
void SManagerAssign( ExtdAddr_t longId, zTableIndex_t zapIndex ) {

  list_add( longId.uint64, zapIndex );
  CotaError_t result = WriteZapToNvm( longId, zapIndex );
  if ( result != COTA_ERROR_NONE )
    printf( "WriteZapToNvm crashed and burned %02d\n", result );

};  // SMananagerAssign


//-----------------------------------------------------------------------------
void list_add( uint64_t uint64, zTableIndex_t zapIndex ) {

  rx_t *rx = (rx_t*)( malloc( sizeof( rx_t ) ) );
  rx->longId.uint64 = uint64;
  the_list[ zapIndex ] = rx;

};  // end list_add


//-----------------------------------------------------------------------------
void list_del( uint16_t index ) {

  if ( the_list[ index ] == NULL ) return;
  free( the_list[ index ] );
  the_list[ index ] = NULL;

};  // end list_del


//-----------------------------------------------------------------------------
void list_del_all( ) {

  for ( size_t iter = 0; iter < LIST_SIZE; ++iter ) list_del( iter );

};  // end list_del_all


//-----------------------------------------------------------------------------
void dump_list( ) {

  char* hex = "";

  for ( size_t index = 0; index < LIST_SIZE; ++index ) {
    if ( the_list[ index ] == 0 ) hex = "Empty";
    else hex = as_hex( the_list[ index ]->longId );
    printf( "%02d: %s\n", index, hex );
  };  // end for loop
  printf( "\n" );

};  // end dump_list
