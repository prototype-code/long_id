#ifndef __ZAP_TABLE_H__
#define __ZAP_TABLE_H__

// Header Files
//-----------------------------------------------------------------------------
#include <stdint.h>  // uint16_t


// Specialty Types
typedef uint16_t zTableIndex_t;


#endif  // __ZAP_TABLE_H__
