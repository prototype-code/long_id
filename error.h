#ifndef __ERROR_H__
#define __ERROR_H__

// Define Error Messages
typedef enum _CotaError_t {

  COTA_ERROR_NONE,
  COTA_ERROR_NVM_MEMORY_FULL,
  COTA_ERROR_NVM_INVALID_DELETE,
  COTA_ERROR_NVM_WRITE,
  COTA_ERROR_NVM_READ,
  COTA_ERROR_ZAP_MAXIMUM,
  COTA_ERROR_COUNT

} CotaError_t;

#endif  // __ERROR_H__
